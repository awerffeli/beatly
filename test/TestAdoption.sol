pragma solidity ^0.4.2;

import "truffle/Assert.sol";
import "truffle/DeployedAddresses.sol";
import "../contracts/Adoption.sol";

contract TestAdoption {
    Adoption adoption = Adoption(DeployedAddresses.Adoption());

    function testUserCanAdoptTest() public {
        uint returnedId = adoption.adopt(8);
        uint expected = 8;

        Assert.equal(returnedId, expected, "Adoption of pet ID 8 should be recorded.");
    }

    function testGetAdopterAddressbyPetId() public {
        address expected = this;

        address adopter = adoption.adopters(8);

        Assert.equal(adopter, expected, "owner of pet ID 8 sould be recorded.");
    }

    function testGetAdopterAddressByPetIdArray() public {
        address expected = this;
        address[16] memory adopters = adoption.getAdopters();

        Assert.equal(adopters[8], expected, "owner of pet ID 8 sould be recorded.");
    }

    function testLike() public {
        adoption.like(5);
        adoption.like(5);
        adoption.like(6);

        uint voteCount1 = adoption.getSongsUpVotes()[4];
        Assert.equal(voteCount1, 0, "No Vote of item ID 4 should be recorded.");

        uint voteCount2 = adoption.getSongsUpVotes()[5];
        Assert.equal(voteCount2, 2, "Vote of item ID 5 should be recorded.");

        uint voteCount3 = adoption.getSongsUpVotes()[6];
        Assert.equal(voteCount3, 1, "Vote of item ID 6 should be recorded.");
    }

    function testLikeEmitEvent() public {
        adoption.like(7);


    }


}