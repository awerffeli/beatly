pragma solidity ^0.4.2;

contract Adoption {

    address public contractCreator;

    uint constant MAX_ITEM_COUNT = 16;
    address[MAX_ITEM_COUNT] public songs;

    struct Song {
        address[] upVoters;
    }

   Song[MAX_ITEM_COUNT] private songsUpVotes;

    struct Artist {
        uint artistId;
        mapping (address => uint) donators;
        uint totalDonationAmount;
        bool initialized;
        address artistAddress;

    }

    mapping (uint => Artist) artistBalances;

    modifier restrictToContractCreator() {
        require(msg.sender == contractCreator);
        _;
    }

  event UpVoteEvent(uint itemId, uint voteCount);


    function Adoption() public {
        contractCreator = msg.sender;

        for (uint i = 0; i < MAX_ITEM_COUNT; i++) {
            addArtist(i, 0x627306090abaB3A6e1400e9345bC60c78a8BEf57);
        }
    }

    function support(uint itemId) public returns (uint)  {
        require(itemId >= 0 && itemId < MAX_ITEM_COUNT);

        songs[itemId] = msg.sender;

        return itemId;
    }

    function addArtist(uint artistId, address artistAddress) public restrictToContractCreator {
        artistBalances[artistId] = Artist({artistId: artistId,initialized: true, totalDonationAmount: 0, artistAddress: artistAddress});
    }

    function supportArtist(uint artistId) public payable {
        Artist storage artist = artistBalances[artistId];
        uint msgValue = msg.value;
        artist.donators[msg.sender] = msgValue;
        artist.totalDonationAmount = artist.totalDonationAmount + msgValue;
    }

    function getArtistDonationList() public view returns (uint[MAX_ITEM_COUNT]) {

        uint[MAX_ITEM_COUNT] memory artistList;

        for (uint i = 0; i < MAX_ITEM_COUNT; i++) {
            artistList[i] = artistBalances[i].totalDonationAmount;
        }

        return artistList;
    }

    function like(uint itemId) public returns (uint) {
        songsUpVotes[itemId].upVoters.push(msg.sender);
        Song memory song = songsUpVotes[itemId];
        uint voteCount = song.upVoters.length;

        emit UpVoteEvent(itemId, voteCount);

        return itemId;
    }

    function getSupporters() public view returns (address[MAX_ITEM_COUNT]) {
        return songs;
    }

    function getSongsUpVotes() public view returns (uint[MAX_ITEM_COUNT]) {
        return mapSongsToVotes();
    }

    function mapSongsToVotes() internal view returns(uint[MAX_ITEM_COUNT]) {
        uint[MAX_ITEM_COUNT] memory songVotesMapping;

        for (uint i = 0; i < songsUpVotes.length; i++) {
            songVotesMapping[i] = songsUpVotes[i].upVoters.length;
        }

        return songVotesMapping;
    }
}