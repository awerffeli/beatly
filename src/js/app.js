App = {
  web3Provider: null,
  contracts: {},
    likeEventinitialized : false,
    debug : false,

  init: function() {
    // Load pets.
    $.getJSON('../pets.json', function(data) {
      var petsRow = $('#petsRow');
      var petTemplate = $('#petTemplate');

      for (i = 0; i < data.length; i ++) {
        petTemplate.find('.panel-title').text(data[i].name);
        petTemplate.find('img').attr('src', data[i].picture);
        petTemplate.find('.pet-breed').text(data[i].breed);
        petTemplate.find('.pet-age').text(data[i].age);
        petTemplate.find('.pet-location').text(data[i].location);
        petTemplate.find('.btn-support').attr('data-id', data[i].id);
        petTemplate.find('.btn-like').attr('data-id', data[i].id);
        petTemplate.find('.btn-support-artist').attr('data-id', data[i].id);
        petTemplate.find('.sound').attr('src', data[i].source);

        petsRow.append(petTemplate.html());
      }
    });

    return App.initWeb3();
  },

  initWeb3: function() {
    if(typeof web3 !== 'undefined') {
      App.web3Provider = web3.currentProvider;
    }
    else {
      App.web3provider = new Web3.providers.HttpProvider('http://localhost:7545');
    }

    web3 = new Web3(App.web3Provider);

    return App.initContract();
  },

    initContract: function() {

        $.getJSON('Adoption.json', function(data) {
          var AdoptionArtifact = data;

          App.contracts.Adoption = TruffleContract(AdoptionArtifact);

          App.contracts.Adoption.setProvider(App.web3Provider);

          return App.initLogAllEvents();
        });


        return App.bindEvents();
    },

    bindContractEvents: function(adopters, account) {

        var adoptionInstance;

        App.contracts.Adoption.deployed().then(function (instance) {

            adoptionInstance = instance;

            /*
            instance.UpVoteEvent().watch(function(error, response) {
                if(!error) {
                    var itemId = response.args.itemId;
                    var voteCount = response.args.voteCount;
                    if (App.likeEventinitialized == true) {
                            $('#main-alert').fadeIn().text("Someone just liked song #" + itemId + ". It already has " + voteCount + " likes!")
                                .delay(6000).fadeOut();

                            $('.panel-pet').eq(itemId).find('.btn-like').text('Like (' + voteCount + ')');

                        App.likeEventinitialized = false;
                    }
                    console.log("itemId "+itemId);
                    console.log("voteCount "+voteCount);
                }
                else {
                    console.error(error);
                }
            });
            */

            instance.UpVoteEvent().watch((error, response) => {

                if(!error) {
                    var itemId = response.args.itemId;
                    var voteCount = response.args.voteCount;
                    if (App.likeEventinitialized == true) {
                            $('#main-alert').fadeIn().text("Someone just liked song #" + itemId + ". It already has " + voteCount + " likes!")
                                .delay(6000).fadeOut();

                            $('.panel-pet').eq(itemId).find('.btn-like').text('Like (' + voteCount + ')');

                        App.likeEventinitialized = false;
                    }
                }
                else {
                    console.error(error);
                }
            });

        });
    },

  bindEvents: function() {
    $(document).on('click', '.btn-support', App.handleSupport);
    $(document).on('click', '.btn-support-artist', App.handleSupportArtist);
    $(document).on('click', '.btn-like', App.handleLike);
  },

    showArtistTotalDonationAmount: function(supporters, accounts) {
            var adoptionInstance;

            App.contracts.Adoption.deployed().then(function(instance) {
                adoptionInstance = instance;

                return adoptionInstance.getArtistDonationList.call();
            }).then(function(artistsList) {
                for(i = 0; i < artistsList.length; i++) {
                    var etherValue = web3.fromWei(artistsList[i], 'ether');
                    $('.panel-pet').eq(i).find('.display-artist-donation-amount').text(etherValue + " ether");
                }
            }).catch(function(err) {
                console.error(err.message);
            });

            return App.bindContractEvents();
    },

    initLogAllEvents: function(supporters, account) {

        if(App.debug == true) {
            App.contracts.Adoption.deployed().then(function(instance) {
                var events =  instance.allEvents();

                // watch for changes
                events.watch(function(error, event){
                    if (!error)
                        console.log(event);
                });
            });
        }

        return  App.markSupportedItems();
    },

  markSupportedItems: function(supporters, account) {

    var adoptionInstance;

    App.contracts.Adoption.deployed().then(function(instance) {

        adoptionInstance = instance;

        return adoptionInstance.getSupporters.call();

    }).then(function(supporters) {
    for(i = 0; i < supporters.length; i++) {
        //todo: check if i am person who supported
      if(supporters[i] !== '0x0000000000000000000000000000000000000000') {
          $('.panel-pet').eq(i).find('.btn-support').text('Supported').attr('disabled', true);
      }
    }

    }).catch(function(err) {
      console.error(err.message);
    });

    return App.markLikedItems();
  },


  markLikedItems: function(supporters, account) {

      var adoptionInstance;

      App.contracts.Adoption.deployed().then(function(instance) {

          adoptionInstance = instance;
          return adoptionInstance.getSongsUpVotes.call();

      }).then(function(songs) {
          for(i = 0; i < songs.length; i++) {
            $('.panel-pet').eq(i).find('.btn-like').text('Like (' + songs[i] + ')');
          }

      }).catch(function(err) {
          console.error(err.message);
      });

      return App.showArtistTotalDonationAmount();
    },

  handleSupport: function(event) {
    event.preventDefault();

    var itemid = parseInt($(event.target).data('id'));

    var adoptionInstance;

    web3.eth.getAccounts(function(error, accounts) {
        if(error) {
          console.error(error);
        }

        var account = accounts[0];

        App.contracts.Adoption.deployed().then(function(instance) {
          adoptionInstance = instance;

          return adoptionInstance.support(itemid, {from: account});
        }).then(function(result) {
          return App.markSupportedItems();
        }).catch(function(err) {
          console.error(err.message);
        });
      });
  },

    handleSupportArtist: function(event) {
        event.preventDefault();

        //var artistAddress = parseInt($(event.target).data('artist-address'));
        var artistId = parseInt($(event.target).data('id'));
        var donationAmount = $('.panel-pet').eq(artistId).find('.donation-amount').val();

        var adoptionInstance;

        web3.eth.getAccounts(function(error, accounts) {
            if(error) {
                console.error(error);
            }

            var account = accounts[0];

            App.contracts.Adoption.deployed().then(function(instance) {
                adoptionInstance = instance;

                return adoptionInstance.supportArtist(artistId, {from: account, value: web3.toWei(donationAmount, 'ether'), gas: 500000, gasPrice: 5000000000000});
            }).then(function(result) {
              // return App.markSupportedItems();
            }).catch(function(err) {
                console.error(err.message);
            });
        });
    },

    handleLike: function(event) {
      event.preventDefault();

      var itemId = parseInt($(event.target).data('id'));
      var adoptionInstance;

      web3.eth.getAccounts(function(error, accounts) {
          if(error) {
              console.error(error);
          }

          var account = accounts[0];

          App.contracts.Adoption.deployed().then(function(instance) {
              adoptionInstance = instance;
              //todo: handle js error when not logged into metamask
              App.likeEventinitialized = true;
              return adoptionInstance.like(itemId, {from: account});
          }).catch(function(err) {
              console.error(err.message);
          });
      });
    }
};

$(function() {
  $(window).load(function() {
    App.init();
  });
});
